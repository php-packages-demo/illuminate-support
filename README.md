# illuminate-support

[illuminate/support](https://packagist.org/packages/illuminate/support)

* [*If you are writing a framework agnostic package, don’t use illuminate/support*
  ](https://mattallan.me/posts/dont-use-illuminate-support/)
  2016-04 Matt Allan

